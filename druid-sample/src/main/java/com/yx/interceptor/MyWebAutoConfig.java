package com.yx.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author yangxi
 */
@Configuration
public class MyWebAutoConfig implements WebMvcConfigurer {

    @Autowired
    private DynamicDatasourceInterceptor dynamicDatasourceInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(dynamicDatasourceInterceptor).addPathPatterns("/**");
    }
}
