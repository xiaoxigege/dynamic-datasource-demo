package com.yx.dto;

import lombok.Data;

@Data
public class UserDto {

    private Integer id;

    private String name;

    private Integer age;
}
