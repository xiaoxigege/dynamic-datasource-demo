package com.yx.mapper;

import com.yx.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserMapper {

    @Select("SELECT * FROM user where age > #{age}")
    List<User> selectUsers(@Param("age") Integer age);

    @Insert("INSERT INTO user (name,age) values (#{name},#{age})")
    boolean addUser(String name, Integer age);

    @Delete("DELETE from user where id = #{id}")
    void deleteUserById(Long id);
}
