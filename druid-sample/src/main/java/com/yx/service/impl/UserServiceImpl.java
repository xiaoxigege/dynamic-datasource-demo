package com.yx.service.impl;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.yx.entity.User;
import com.yx.mapper.UserMapper;
import com.yx.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public List<User> selectMasterUsers() {
        return userMapper.selectUsers(1);
    }

    @DS("slave_1")
    @Override
    public List<User> selectSlaveUsers() {
        return userMapper.selectUsers(1);
    }

    @DS("slave_2")
    @Override
    public List<User> selectSlave2Users() {
        return userMapper.selectUsers(1);
    }

    @Override
    public void addUser(User user) {
        userMapper.addUser(user.getName(), user.getAge());
    }

    @Override
    public void deleteUserById(Long id) {
        userMapper.deleteUserById(id);
    }
}
