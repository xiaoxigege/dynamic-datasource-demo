package com.yx.service;


import com.yx.entity.User;

import java.util.List;

public interface UserService {

    List<User> selectMasterUsers();

    List<User> selectSlaveUsers();

    List<User> selectSlave2Users();

    void addUser(User user);

    void deleteUserById(Long id);
}
